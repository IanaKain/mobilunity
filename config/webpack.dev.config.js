// eslint-disable-next-line import/order
const merge = require('webpack-merge');
const paths = require('./paths');
const baseConfig = require('./webpack.base.config');


const devConfiguration = () => merge([
  {
    mode: 'development',
    output: {
      publicPath: paths.dev.projectPublicPath,
    },
    devtool: 'eval-sourcemap',
    optimization: {
      runtimeChunk: 'single',
    },
    devServer: {
      historyApiFallback: true,
      compress: true,
      stats: 'errors-only',
      progress: true,
      overlay: true,
      contentBase: paths.projectSrc,
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' }
          ],
        }
      ],
    },
  }
]);

module.exports = () => merge(baseConfig(), devConfiguration());
