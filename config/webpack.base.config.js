const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const paths = require('./paths');


module.exports = () => merge([
  {
    entry: ['@babel/polyfill', paths.projectSrc],
    output: {
      pathinfo: true,
      filename: paths.appJS,
    },
    resolve: {
      modules: ['node_modules'],
      extensions: paths.moduleFileExtensions.map(ext => `.${ext}`),
    },
    module: {
      strictExportPresence: true,
      rules: [
        { parser: { requireEnsure: false } },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          include: paths.projectSrc,
          use: { loader: 'babel-loader' },
        }
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: paths.appHtml,
        filename: './index.html',
        minify: {
          collapseWhitespace: true,
        },
      })
    ],
  }
]);
