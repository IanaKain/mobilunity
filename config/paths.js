const path = require('path');
const fs = require('fs');


const projectDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(projectDirectory, relativePath);

const moduleFileExtensions = [
  'web.mjs',
  'mjs',
  'web.js',
  'js',
  'json',
  'web.jsx',
  'jsx'
];

module.exports = {
  projectSrc: resolveApp('src'),
  appHtml: resolveApp('public/index.html'),
  appBuild: resolveApp('build'),
  appJS: './js/bundle.js',
  dev: {
    projectPublicPath: '/',
  },
};

module.exports.moduleFileExtensions = moduleFileExtensions;
