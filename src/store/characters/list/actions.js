import { createActions } from '../../../utils/store';


const listActions = createActions([
  'FETCH',
  'FETCH_SUCCESS',
  'FETCH_ERROR',
  'FETCH_ERROR_SHOW',
  'FETCH_ERROR_HIDE',
  'SET_FILTER',
  'CLEAR_FILTER'
], 'list');


export { listActions };
