const charactersListSelector = state => state.list && state.list.characters;
const filterSelector = state => state.list && state.list.filter;
const listLoadingSelector = state => state.list && state.list.loading;
const listErrorSelector = state => state.list && state.list.errors;


const filteredList = state => {
  const list = charactersListSelector(state);
  const filter = filterSelector(state);

  if (!filter || !list) { return list; }

  return list.filter(character => {
    const name = character.name.toLowerCase();

    return name.includes(filter);
  });
};

export { filteredList, listLoadingSelector, listErrorSelector };