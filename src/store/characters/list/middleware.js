import { parseResponse, createErrorMessage } from '../../../utils';
import { createMiddleware } from '../../../utils/store';
import { charactersUrl } from '../../../constants';

import { listActions } from './actions';


const listMiddleware = ({ dispatch }) => createMiddleware({
  [listActions.FETCH]: () => fetch(charactersUrl)
    .then(parseResponse)
    .then(response => dispatch(listActions.fetchSuccess(response.results)))
    .catch(error => dispatch(listActions.fetchError(error))),

  [listActions.FETCH_ERROR]: error => {
    const id = new Date().getTime();

    dispatch(listActions.fetchErrorShow({ id, error: createErrorMessage(error) }));
    setTimeout(() => dispatch(listActions.fetchErrorHide(id)), 3000);
  },
});

export { listMiddleware };
