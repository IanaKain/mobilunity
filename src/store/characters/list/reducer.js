import { listActions } from './actions';


const initialState = {
  characters: null,
  filter: '',
  errors: [],
  loading: false,
};

const listReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case listActions.FETCH:
      return { ...state, loading: true };
    case listActions.FETCH_SUCCESS:
      return { ...state, loading: false, characters: payload };
    case listActions.FETCH_ERROR_SHOW:
      return { ...state, loading: false, errors: state.errors.concat(payload) };
    case listActions.FETCH_ERROR_HIDE:
      return { ...state, loading: false, errors: state.errors.filter(({ id }) => id !== payload) };
    case listActions.SET_FILTER:
      return { ...state, filter: payload };
    case listActions.CLEAR_FILTER:
      return { ...state, filter: '' };
    default:
      return state;
  }
};

export { listReducer };
