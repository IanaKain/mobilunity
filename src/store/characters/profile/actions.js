import { createActions } from '../../../utils/store';


const profileActions = createActions([
  'FETCH',
  'FETCH_SUCCESS',
  'FETCH_ADDITIONAL_DATA',
  'FETCH_ADDITIONAL_DATA_SUCCESS',
  'FETCH_ERROR',
  'FETCH_ERROR_SHOW',
  'FETCH_ERROR_HIDE'
], 'profile');

export { profileActions };
