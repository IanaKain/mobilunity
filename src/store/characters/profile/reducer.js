import { profileActions } from './actions';


const initialState = {
  character: null,
  errors: [],
  loading: false,
  dataloading: false,
};

export const profileReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case profileActions.FETCH:
      return { ...state, loading: true };
    case profileActions.FETCH_SUCCESS:
      return { ...state, loading: false, character: payload };
    case profileActions.FETCH_ADDITIONAL_DATA:
      return { ...state, dataloading: true };
    case profileActions.FETCH_ADDITIONAL_DATA_SUCCESS:
      return { ...state, character: { ...state.character, ...payload }, dataloading: false };
    case profileActions.FETCH_ERROR_SHOW:
      return { ...state, loading: false, dataloading: false, errors: state.errors.concat(payload) };
    case profileActions.FETCH_ERROR_HIDE:
      return { ...state, loading: false, dataloading: false, errors: state.errors.filter(({ id }) => id !== payload) };
    default:
      return state;
  }
};
