import { createErrorMessage, parseResponse } from '../../../utils';
import { createMiddleware } from '../../../utils/store';
import { additionalProfileData } from '../../../constants';

import { profileActions } from './actions';


const profileMiddleware = ({ dispatch }) => createMiddleware({
  [profileActions.FETCH]: url => fetch(url)
    .then(parseResponse)
    .then(character => dispatch(profileActions.fetchSuccess(character)))
    .catch(error => dispatch(profileActions.fetchError(error))),

  [profileActions.FETCH_SUCCESS]: character => additionalProfileData.forEach(key => {
    const listToFetch = Array.isArray(character[key]) ? character[key] : [character[key]];

    dispatch(profileActions.fetchAdditionalData({ [key]: listToFetch }));
  }),

  [profileActions.FETCH_ADDITIONAL_DATA]: dataToFetch => {
    const [key] = Object.keys(dataToFetch);

    return Promise
      .all(dataToFetch[key].map(url => fetch(url).then(parseResponse)))
      .then(data => dispatch(profileActions.fetchAdditionalDataSuccess({ [key]: data })))
      .catch(error => dispatch(profileActions.fetchError(error)));
  },

  [profileActions.FETCH_ERROR]: error => {
    const id = new Date().getTime();

    dispatch(profileActions.fetchErrorShow({ id, error: createErrorMessage(error) }));
    setTimeout(() => dispatch(profileActions.fetchErrorHide(id)), 3000);
  },
});

export { profileMiddleware };
