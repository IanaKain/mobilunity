import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import { listReducer } from './characters/list/reducer';
import { profileReducer } from './characters/profile/reducer';
import { profileMiddleware } from './characters/profile/middleware';
import { listMiddleware } from './characters/list/middleware';


const storeEnhancers = [];
const history = createBrowserHistory();

const { devToolsExtension } = window;

if (typeof devToolsExtension === 'function') {
  storeEnhancers.push(devToolsExtension());
}

const middleware = [
  thunk,
  routerMiddleware(history),
  listMiddleware,
  profileMiddleware
];

const rootReducer = combineReducers({
  router: connectRouter(history),
  list: listReducer,
  profile: profileReducer,
});

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...storeEnhancers
);

const store = createStore(
  rootReducer,
  {},
  composedEnhancers
);

export { history, store };
