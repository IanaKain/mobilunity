const messages = {
  title: 'Search All Star Wars People',
  placeholder: 'e.g. Luke Skywalker',
  hint: 'Start typing to see the result',
  header: 'The Star Wars Characters',
};

const additionalProfileData = ['films', 'homeworld', 'species', 'starships', 'vehicles'];

const numberPattern = /\d+/g;

const baseUrl = 'https://swapi.co/api';
const paths = {
  characters: '/people',
};
const charactersUrl = `${baseUrl}${paths.characters}`;

export {
  messages,
  baseUrl,
  charactersUrl,
  numberPattern,
  additionalProfileData
};
