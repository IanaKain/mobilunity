import PropTypes from 'prop-types';


const characterPropType = PropTypes.shape({
  name: PropTypes.string,
  height: PropTypes.string,
  mass: PropTypes.string,
  hair_color: PropTypes.string,
  skin_color: PropTypes.string,
  eye_color: PropTypes.string,
  birth_year: PropTypes.string,
  gender: PropTypes.string,
  homeworld: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  films: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  species: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  vehicles: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  starships: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  created: PropTypes.string,
  edited: PropTypes.string,
  url: PropTypes.string,
});

const errorPropType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.shape({
    id: PropTypes.number,
    error: PropTypes.string,
  })
]);

export { characterPropType, errorPropType };
