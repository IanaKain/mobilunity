import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import { store, history } from './store/store';
import { Main } from './view';
import { routes } from './routes';

import 'antd/dist/antd.css';
import './styles/index.css';


const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path={routes.characters} component={Main} />
        <Redirect exact from={routes.root} to={routes.characters} />
        <Redirect from="*" to={routes.characters} />
      </Switch>
    </ConnectedRouter>
  </Provider>
);

export { App };
