const __capitalizeType = type => type
  .split('_')
  .map((part, index) => {
    const lower = part.toLowerCase();
    const capital = part.charAt(0).toUpperCase() + lower.substr(1);

    return index ? capital : lower;
  })
  .join('');

const createMiddleware = actionTypes => next => action => {
  const currectAction = actionTypes[action.type];
  const { payload } = action;

  if (currectAction) {
    currectAction(payload);
  }

  return next(action);
};

const createActions = (types, prefix) => {
  const delimiter = '/';

  return types.reduce((actions, type) => {
    const action = prefix.concat(delimiter).concat(type);

    return {
      ...actions,
      [type.toUpperCase()]: action,
      [__capitalizeType(type)]: payload => payload
        ? ({ type: action, payload })
        : ({ type: action }),
    };
  }, {});
};

export { createMiddleware, createActions };
