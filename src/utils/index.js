import pathToRegexp from 'path-to-regexp';


const getJSON = response => {
  const contentType = response.headers.get('content-type');

  if (contentType && contentType.includes('application/json')) {
    return response.json();
  }
  throw new TypeError("Oops, we haven't got JSON!");
};

const parseResponse = response => {
  const { status } = response;

  if (status >= 200 && status < 300) {
    return getJSON(response);
  }

  return getJSON(response).then(({ error }) => {
    throw error;
  });
};

const generatorCache = new Map();
const generatorCacheLimit = 10000;

function generatePath(path, params = {}) {
  let generator;

  if (generatorCache.has(path)) {
    generator = generatorCache.get(path);
  } else {
    generator = pathToRegexp.compile(path);

    if (generatorCache.size < generatorCacheLimit) {
      generatorCache.set(path, generator);
    }
  }

  return generator(params);
}

const capitalizeText = text => {
  if (typeof text !== 'string') {
    throw new Error('You must provide a string');
  }
  const capital = text.charAt(0).toUpperCase();
  const lower = text.substr(1);

  return capital + lower;
};

const createErrorMessage = e => typeof e === 'string' ? e : e.message;

export {
  parseResponse,
  generatePath,
  createErrorMessage,
  capitalizeText
};
