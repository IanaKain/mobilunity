const routes = {
  root: '/',
  characters: '/characters',
  character: '/characters/:id',
};

export { routes };
