import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { routes } from '../routes';

import { CharacterList } from './PageComponents/CharacterList';
import { CharacterProfile } from './PageComponents/CharacterProfile';


const Main = () => (
  <Switch>
    <Route exact path={routes.characters} render={() => <CharacterList />} />
    <Route path={routes.character} component={CharacterProfile} />
  </Switch>
);

export { Main };
