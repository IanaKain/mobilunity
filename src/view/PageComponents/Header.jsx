import React from 'react';


const Header = ({ children }) => (
  <header className="header">
    {typeof children === 'string'
      ? <div className="header-content">{children}</div>
      : children
    }
  </header>
);

export { Header };
