import React from 'react';


const Error = ({ errors }) => (
  <div className="search__error" >
    {errors.map(({ error, id }) => (
      <p key={id} className="error__description">{error}</p>
    ))}
  </div>
);

export { Error };
