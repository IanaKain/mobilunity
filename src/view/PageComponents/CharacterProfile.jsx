import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tag, Card, List } from 'antd';

import { charactersUrl, messages } from '../../constants';
import { profileActions } from '../../store/characters/profile/actions';
import { capitalizeText } from '../../utils';
import { characterPropType, errorPropType } from '../../constants/prop-types';

import { Error } from './Error';
import { Header } from './Header';


const { Meta } = Card;

const mapStateToProps = state => ({
  character: state.profile.character,
  loading: state.profile.loading,
  dataloading: state.profile.dataloading,
  errors: state.profile.errors,
});

const mapDispatchToProps = dispatch => ({
  fetchCharacter: url => dispatch(profileActions.fetch(url)),
});

const renderData = data => Array.isArray(data)
  ? data.map(({ name, title }, i) => <div key={i}>{title || name}</div>)
  : data;

const profileContent = [{
  title: 'Gender',
  key: 'gender',
}, {
  title: 'Hair color',
  key: 'hair_color',
}, {
  title: 'Height',
  key: 'height',
}, {
  title: 'Mass',
  key: 'mass',
}, {
  title: 'Skin color',
  key: 'skin_color',
}, {
  title: 'Birth year',
  key: 'birth_year',
}, {
  title: 'Eye color',
  key: 'eye_color',
}, {
  title: 'Homeworld',
  key: 'homeworld',
  render: renderData,
}, {
  title: 'Films',
  key: 'films',
  render: renderData,
}, {
  title: 'Starships',
  key: 'starships',
  render: renderData,
}, {
  title: 'Vehicles',
  key: 'vehicles',
  render: renderData,
}];

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
class CharacterProfile extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    fetchCharacter: PropTypes.func.isRequired,
    character: characterPropType,
    loading: PropTypes.bool.isRequired,
    dataloading: PropTypes.bool.isRequired,
    errors: PropTypes.arrayOf(errorPropType),
  };

  async componentDidMount() {
    const { match: { params } } = this.props;

    await this.props.fetchCharacter(`${charactersUrl}/${params.id}`);
  }

  renderCharacterProfileItem = character => ({ key, title, render }) => {
    let description = '';

    if (render) {
      const result = render(character[key]);

      if (result && result.length) {
        description = result;

        return (
          <List.Item>
            <List.Item.Meta title={title} description={description}/>
          </List.Item>
        );
      }
    }

    description = character[key];

    return (
      <List.Item>
        <List.Item.Meta title={title} description={
          typeof description === 'string' ? capitalizeText(description) : description}/>
      </List.Item>
    );
  };

  render() {
    const { character, errors, loading, dataloading } = this.props;
    const errorToShow = Boolean(errors.length) && <Error errors={errors} />;
    const showProfile = Boolean(!errors.length && character);

    return (
      <React.Fragment>
        <Header>{messages.header}</Header>
        <main className="main">
          {errorToShow}
          <Card style={{
            width: 782,
            marginTop: 20,
            marginLeft: 'auto',
            marginRight: 'auto',
          }} loading={loading || dataloading}>
            <Meta
              title={character && character.name}
              style={{ marginBottom: 20 }}
              description={
                character &&
                character.species.map(({ name }, i) => <Tag key={i} color="cyan">{name}</Tag>)
              }
            />

            {showProfile && (
              <List
                itemLayout="horizontal"
                dataSource={profileContent}
                renderItem={this.renderCharacterProfileItem(character)}
              />)}
          </Card>
        </main>
      </React.Fragment>
    );
  }
}

export { CharacterProfile };
