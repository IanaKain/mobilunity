import React from 'react';
import { connect } from 'react-redux';

import { messages } from '../../constants';
import { listActions } from '../../store/characters/list/actions';
import { filteredList } from '../../store/characters/list/selectors';


const mapStateToProps = state => ({
  list: filteredList(state),
});

const mapDispatchToProps = dispatch => ({
  setFilter: value => dispatch(listActions.setFilter(value)),
  clearFilter: () => dispatch(listActions.clearFilter()),
});

@connect(mapStateToProps, mapDispatchToProps)
class Search extends React.Component {
  state = {
    value: '',
  };

  handleInput = ({ target: { value } }) => {
    this.setState({ value }, () => {
      this.props.setFilter(value.trim().toLowerCase());
    });
  };

  onSubmit = e => {
    e.preventDefault();
    // clear input on Enter press
    this.setState({ value: '' }, () => {
      this.props.clearFilter();
    });
  };

  render() {
    const { value } = this.state;
    const { disabled } = this.props;

    return (
      <form className="search" onSubmit={this.onSubmit}>
        <h1 className="search__title">{messages.title}</h1>
        <input
          disabled={disabled}
          className="search__input"
          placeholder={messages.placeholder}
          onChange={this.handleInput}
          value={value}
        />
      </form>
    );
  }
}

export { Search };
