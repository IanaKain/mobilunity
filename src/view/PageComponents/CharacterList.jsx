import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { List, Spin } from 'antd';

import { generatePath } from '../../utils';
import { routes } from '../../routes';
import {
  filteredList,
  listErrorSelector,
  listLoadingSelector
} from '../../store/characters/list/selectors';
import { listActions } from '../../store/characters/list/actions';
import { characterPropType, errorPropType } from '../../constants/prop-types';
import { numberPattern, messages } from '../../constants';

import { Search } from './Search';
import { Error } from './Error';
import { Header } from './Header';


const mapStateToProps = state => ({
  list: filteredList(state),
  loading: listLoadingSelector(state),
  errors: listErrorSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchList: () => dispatch(listActions.fetch()),
});

@connect(mapStateToProps, mapDispatchToProps)
class CharacterList extends React.Component {
  static propTypes = {
    fetchList: PropTypes.func.isRequired,
    list: PropTypes.arrayOf(characterPropType),
    loading: PropTypes.bool.isRequired,
    errors: PropTypes.arrayOf(errorPropType),
  };

  componentDidMount() {
    this.props.fetchList();
  }

  renderCharacter = ({ name, url }) => {
    const [id] = url.match(numberPattern);
    const path = generatePath(routes.character, { id });

    return (
      <List.Item className="page-list__item">
        <Link key={name} to={path}>{name}</Link>
      </List.Item>
    );
  };

  render() {
    const { list, errors, loading } = this.props;
    const errorToShow = Boolean(errors.length) && <Error errors={errors} />;
    const showList = Boolean(!errors.length && (list && list.length));

    return (
      <React.Fragment>
        <Header>
          <Search disabled={!showList} />
        </Header>
        <main className="main">
          {errorToShow}
          <Spin tip="Loading..." spinning={!showList} className="page-spinner" />
          {showList && (
            <List
              className="page-list"
              size="large"
              loading={loading}
              header={<div>{messages.header}</div>}
              bordered
              dataSource={list}
              renderItem={this.renderCharacter}
            />
          )}
        </main>
      </React.Fragment>
    );
  }
}

export { CharacterList };
